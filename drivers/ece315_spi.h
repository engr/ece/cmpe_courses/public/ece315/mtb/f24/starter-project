/*
 *  Created on: Jan 18, 2022
 *      Author: Joe Krachey
 */

#ifndef SPI_H__
#define SPI_H__

#include "cy_pdl.h"
#include "cyhal.h"
#include "cybsp.h"

#include "ece315_pins.h"

/* Macros */
#define SPI_FREQ			1000000

/* Public Global Variables */
extern cyhal_spi_t mSPI;

/* Public API */
cy_rslt_t ece315_spi_init(void);

#endif 
