/**
 * @file ece353.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-08-15
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef __ECE315_CONSOLE_H__
#define __ECE315_CONSOLE_H__

#include "cy_pdl.h"
#include "cyhal.h"
#include "cybsp.h"
#include "cy_retarget_io.h"

#include <ctype.h>
#include <stdio.h>

#include "ece315_pins.h"

/**
 * @brief 
 * Initialize the UART interface used to print messages to the serial monitor  
 */
void ece315_console_init(void);

/**
 * @brief 
 * Returns a string entered from the console 
 * @param msg 
 * A pointer to the character array where the string will be written to
 * @return true
 * A string was entered at the console.  The string ends when a \n or \r is
 * received
 * @return false 
 * A string has not been received.
 */
bool ece315_console_rx_string(uint8_t *msg);

#endif