/**
 * @file ece315_pins.h
 * @author Joe Krachey (jkrachey@wisc.edu)
 * @brief 
 * @version 0.1
 * @date 2023-11-01
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef __ECE315_PINS_H__
#define __ECE315_PINS_H__

/* If you are using your own board, you will need to
 * change the define below to be:
 *  #undef ECE315_STAFF_BOARD
 * 
 * You will then need to define each of the following MACROs to 
 * match the pins you used when designing your version of the board
 */
#define ECE315_STAFF_BOARD

#ifdef ECE315_STAFF_BOARD

/*Buzzer*/
#define PIN_BUZZER              P9_0

/*Button*/
#define PIN_BTN                 P9_1

/* SPI Pins*/
#define PIN_SPI_MOSI		    P10_0
#define PIN_SPI_MISO		    P10_1
#define PIN_SPI_CLK		        P10_2

#else

/* Define each of the following pins based on the pins you 
 * selected for your version of the ECE315 board
 */

/*Buzzer*/
#define PIN_BUZZER              P0_0

/*Button*/
#define PIN_BTN                 P0_0

/* SPI Pins*/
#define PIN_SPI_MOSI		    P0_0
#define PIN_SPI_MISO		    P0_1
#define PIN_SPI_CLK             P0_2

#endif

/* UART Pins*/
#define PIN_UART_DBG_RX			P5_0
#define PIN_UART_DBG_TX			P5_1

/* UART PORT*/
#define CONSOLE_PORT            SCB5

#endif
