#ifndef __ECE315_H__
#define __ECE315_H__

#include "cyhal.h"
#include "cy_pdl.h"
#include "cybsp.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>


#include "ece315_pins.h"
#include "ece315_console.h"
#include "ece315_buttons.h"
#include "ece315_buzzer.h"
#include "ece315_timer.h"
#include "ece315_spi.h"

#endif