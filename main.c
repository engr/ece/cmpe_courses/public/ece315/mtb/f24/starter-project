/**
 * @file main.c
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-04-24
 *
 * @copyright Copyright (c) 2024
 *
 */

#include "main.h"

/******************************************************************************
 * Macros
 *******************************************************************************/

/*******************************************************************************
 * Global Variables
 *******************************************************************************/
led_packet_t LEDs[14] =
    {
        {.command = 0x00, .blue = 0x00, .green = 0x00, .red = 0x00}, // LEDs[0]  -- Start of Frame
        {.command = 0x9f, .blue = 0x1f, .green = 0x00, .red = 0x00}, // LEDs[1]
        {.command = 0x9f, .blue = 0x00, .green = 0x1f, .red = 0x00}, // LEDs[2]
        {.command = 0x9f, .blue = 0x00, .green = 0x00, .red = 0x1f}, // LEDs[3]
        {.command = 0x9f, .blue = 0x1f, .green = 0x00, .red = 0x00}, // LEDs[4]
        {.command = 0x9f, .blue = 0x00, .green = 0x1f, .red = 0x00}, // LEDs[5]
        {.command = 0x9f, .blue = 0x00, .green = 0x00, .red = 0x1f}, // LEDs[6]
        {.command = 0x9f, .blue = 0x1f, .green = 0x00, .red = 0x00}, // LEDs[7]
        {.command = 0x9f, .blue = 0x00, .green = 0x1f, .red = 0x00}, // LEDs[8]
        {.command = 0x9f, .blue = 0x00, .green = 0x00, .red = 0x1f}, // LEDs[9]
        {.command = 0x9f, .blue = 0x1f, .green = 0x00, .red = 0x00}, // LEDs[10]
        {.command = 0x9f, .blue = 0x00, .green = 0x1f, .red = 0x00}, // LEDs[11]
        {.command = 0x9f, .blue = 0x00, .green = 0x00, .red = 0x1f}, // LEDs[12]
        {.command = 0xff, .blue = 0xff, .green = 0xff, .red = 0xff}  // LEDs[13] -- End of Frame
};

/*******************************************************************************
 * Function Prototypes
 *******************************************************************************/

/*******************************************************************************
 * Function Definitions
 *******************************************************************************/
/*******************************************************************************
 * Function Name: main
 *********************************************************************************/
int main(void)
{
    cy_rslt_t result;
    uint8_t rx_data[4];
    bool buzzer_on = false;

    /* Initialize the device and board peripherals */
    result = cybsp_init();

    /* Board init failed. Stop program execution */
    if (result != CY_RSLT_SUCCESS)
    {
        CY_ASSERT(0);
    }

    /* Enable global interrupts */
    __enable_irq();

    ece315_console_init();

    ece315_spi_init();

    ece315_button_init();

    ece315_buzzer_init(2500);

    /* Send the Clear Screen Escape Sequence*/
    printf("\x1b[2J\x1b[;H");

    printf("**************************************************\n\r");
    printf("* ECE315 F24 Solutions \n\r");
    printf("* Date: %s\n\r", __DATE__);
    printf("* Time: %s\n\r", __TIME__);
    printf("**************************************************\n\r");

    /* Send the data to the serial LEDs*/
    for (int i = 0; i < 14; i++)
    {
        cyhal_spi_transfer(&mSPI, (uint8_t *)&LEDs[i], 4, rx_data, 4, 0x00);
    }

    for (;;)
    {
        if (ALERT_BUTTON_PRESSED)
        {
            ALERT_BUTTON_PRESSED = false;

            /* Toggle the Buzzer */
            buzzer_on = !buzzer_on;

            if (buzzer_on)
            {
                ece315_buzzer_start();
            }
            else
            {
                ece315_buzzer_stop();
            }
        }
    }
}

/* [] END OF FILE */
